package com.example.inv_clase5

import android.util.Log

// clases pueden ser descritas en una sola linea
// class Perrito (var nombre : String, var apellido : String, var peso : Int)

// las clases y los metodos pueden ser abiertos o cerrados
// default es cerrado (no se puede heredar, no se puede sobreescribir)

// constructor principal es el que esta definido en la línea que tambien define la clase
open class Perrito (var nombre : String, var apellido : String, var peso : Int){

    // propiedades - manera de tener atributos con get y set separados en acceso
    var sobrePeso : Int
    private set

    // el constructor principal no tiene cuerpo

    // hay un bloque de codigo que SIEMPRE se ejecuta - bloque de inicializacion
    init {

        sobrePeso = peso - 10
        Log.wtf("INICIALIZACION", "SI CORRE!")
    }


    // puedes tener todos los constructores que quieras PERO
    // TODOS deben invocar al principal o a otro constructor que invoque al principal

    // constructor en una línea
    constructor(nombre : String) : this(nombre, "Perez", 10)

    // constructor con cuerpo
    constructor(nombre : String, peso : Int) : this(nombre, "Rodriguez", peso){

        // es posible que el constructor tenga lógica extra
        Log.wtf("constructor secundario", "hola!")
    }

    fun suma(a : Int, b : Int) : Int {

        return a + b
    }
}