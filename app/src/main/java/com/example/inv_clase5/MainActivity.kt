

package com.example.inv_clase5

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    val CAPTURA_CODE = 1
    lateinit var imageView : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)

        // declaración de variables
        // 2 opciones - mutables o inmutables
        // otras 2 opciones - con tipo explícito o implicito

        val constante : Int = 2

        // error de compilación:
        // constante = 5

        // con tipo implícito:
        val constante2 = 3

        // variables mutables
        var variable : String = "HOLA"
        var variable2 = "HOLA OTRA VEZ"

        // string template
        var ejemplo = "el valor de variable es: $variable, ${1 + 2}"

        Log.wtf("PRUEBAS", ejemplo)
        Log.wtf("PRUEBAS", "${cuadrado(2)}")
        Log.wtf("PRUEBAS", "${multiplicacion(2, 3)}")

        // invocaciones con valores default
        Log.wtf("PRUEBAS", "${resta(2)}")
        Log.wtf("PRUEBAS", "${resta(2, 1)}")

        // invocacion usando parametros nombrados
        Log.wtf("PRUEBAS", "${resta(2, b = 6)}")

        // kotlin - lenguaje seguro para ejecución
        // evita el asesino de código en ejecución más prominente
        // nulos! NullPointerException

        // 2 categorías de objetos
        // - nullable y non-nullable

        // non nullable
        var noNulo : String = "NO SOY NULO!"
        // noNulo = null // error de compilacion

        // nullable
        var siNulo : String? = "SI FUI NULO :("
        siNulo = null

        // como le hago para evitar nullpointerexception?

        // solucion 1: checar si es null o no
        if(siNulo != null){
            Log.wtf("PRUEBAS", "${siNulo.length}")
        }

        // safe calls - llamada segura
        Log.wtf("PRUEBAS", "${siNulo?.length}")

        // elvis operator
        // inline if para nulls
        // ?:

        var elvis = siNulo?.length ?: -1

        // !! - forzar ejecución sin imoprtar si es nullable
        // var tamanio = siNulo!!.length

        var fido = Perrito("fido", "garcia", 40)
        Log.wtf("PRUEBAS", "${fido.nombre} ${fido.apellido}")

        //fido.apellido = "hernandez"
        //Log.wtf("PRUEBAS", "${fido.nombre} ${fido.apellido}")


        var firulais = Perrito("Firulais");
        Log.wtf("PRUEBAS", "${firulais.nombre} ${firulais.apellido}")

        var kaiser = Perrito("Kaiser", 5);
        Log.wtf("PRUEBAS", "${kaiser.nombre} ${kaiser.apellido}")

        // probando propiedad - lectura sin problemas
        Log.wtf("PRUEBAS", "${kaiser.sobrePeso}")

        // escritura no es posible
        // kaiser.sobrePeso = 2
    }

    // declaracion de funciones
    fun cuadrado(numero : Int) : Int {
        return numero * numero
    }

    // retorno con tipo inferido
    fun multiplicacion(a : Int, b : Int) = a * b

    // parametros default
    fun resta(a : Int, b : Int = 0) : Int {
        return a - b
    }

    public fun tomarFoto(v : View){

        // inicializando intent con acción, no con tipo particular
        var i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if(i.resolveActivity(packageManager) == null)
            return

        startActivityForResult(i, CAPTURA_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CAPTURA_CODE && resultCode == Activity.RESULT_OK){

            val imagen = data?.extras?.get("data") as Bitmap
            imageView.setImageBitmap(imagen)
        }
    }
}